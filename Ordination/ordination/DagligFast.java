package ordination;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {
	private Dosis[] dagligDosis;
	private int givetDosis = 0;

	public DagligFast(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
		dagligDosis = new Dosis[4];
	}

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
		super(startDen, slutDen, laegemiddel);
		dagligDosis = new Dosis[4];

	}

	public void opretDosis(LocalTime klokkeslaet, double antalEnheder) {
		if (givetDosis <= 4) {
			if (klokkeslaet.isAfter(LocalTime.of(23, 59)) || klokkeslaet.isBefore(LocalTime.of(00, 00))) {
				throw new DateTimeException("Du kan ikke give en værdi uden for tidsrummet 00:00 til 23:59");
			} else if (antalEnheder < 0) {
				throw new IllegalArgumentException("Angiv et positivt antal enheder");
			} else {
				Dosis dosis = new Dosis(klokkeslaet, antalEnheder);
				dagligDosis[givetDosis] = dosis;
				givetDosis++;
			}
		} else {
			throw new RuntimeException("Kan ikke give flere daglige dosiser.");
		}
	}

	@Override
	public double samletDosis() {
		double gange = 0.0;
		for (int i = 0; i < dagligDosis.length; i++) {
			if (dagligDosis[i] != null) {
				gange += dagligDosis[i].getAntal();
			}
		}
		return gange * antalDage();
	}

	@Override
	public double doegnDosis() {
		double gange = 0.0;
		for (int i = 0; i < dagligDosis.length; i++) {
			if (dagligDosis[i] != null) {
				gange += dagligDosis[i].getAntal();
			}
		}
		return gange;
	}

	@Override
	public String getType() {
		return "Daglig fast";
	}

	public Dosis[] getDoser() {
		Dosis[] dosisTemp = dagligDosis;
		return dosisTemp;
	}
}
