package ordination;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {

	private double antalEnheder;
	private ArrayList<LocalDate> givetDosis = new ArrayList<LocalDate>();

	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double antalEnheder) {
		super(startDen, slutDen, laegemiddel);
		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers og datoen givesDen ignoreres
	 *
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		boolean givet = false;
		if (givesDen.isAfter(getStartDen()) && givesDen.isBefore(getSlutDen()) || givesDen.isEqual(getStartDen())
				|| givesDen.isEqual(getSlutDen())) {
			givet = true;
			givetDosis.add(givesDen);
		} else {
			throw new DateTimeException("Dato er udenfor Ordinations periode");
		}
		return givet;
	}

	/**
	 * (antal gange ordinationen er anvendt * antal enheder) / (antal dage
	 * mellem første og sidste givning)
	 *
	 * @return double
	 */
	@Override
	public double doegnDosis() {
		double doegnDosis = -1;
		if (givetDosis.isEmpty()) {
			doegnDosis = 0;
		} else {
			doegnDosis = givetDosis.size() * antalEnheder / antalDage();

		}
		return doegnDosis;
	}

	@Override
	public double samletDosis() {
		double doegnDosis = -1;
		if (givetDosis.isEmpty()) {
			doegnDosis = 0;
		} else {
			doegnDosis = givetDosis.size() * antalEnheder;

		}
		return doegnDosis;
	}

	@Override
	public int antalDage() {
		int antalDage = 0;
		LocalDate foerste = null;
		LocalDate sidste = null;
		if (givetDosis.size() == 1) {
			antalDage = 1;
		} else if (givetDosis.size() > 1) {
			foerste = givetDosis.get(0);
			sidste = givetDosis.get(1);
			for (LocalDate date : givetDosis) {
				if (date.isAfter(foerste) && date.isBefore(sidste)) {

				} else if (date.isBefore(foerste)) {
					foerste = date;
				} else {
					sidste = date;
				}
			}
			System.out.println((int) ChronoUnit.DAYS.between(foerste, sidste) + 1);
			antalDage = (int) ChronoUnit.DAYS.between(foerste, sidste) + 1;
		}
		return antalDage;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 *
	 * @return
	 */
	public int getAntalGangeGivet() {
		return givetDosis.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return "PN";
	}

}
