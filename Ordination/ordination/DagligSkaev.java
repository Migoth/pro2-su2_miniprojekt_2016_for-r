package ordination;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> givetDosis = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
		super(startDen, slutDen, laegemiddel);
	}

	public void opretDosis(LocalTime tid, double antal) {
		if (tid.isAfter(LocalTime.of(23, 59)) || tid.isBefore(LocalTime.of(00, 00))) {
			throw new DateTimeException("Du kan ikke give en værdi uden for tidsrummet 00:00 til 23:59");
		} else if (antal < 0) {
			throw new IllegalArgumentException("Angiv et positivt antal enheder");
		} else {
			Dosis dosis = new Dosis(tid, antal);
			givetDosis.add(dosis);
		}
	}

	@Override
	public double samletDosis() {
		double sum = 0.0;
		for (Dosis dosis : givetDosis) {
			sum += dosis.getAntal();
		}
		return sum;
	}

	@Override
	public double doegnDosis() {
		double sum = 0.0;
		for (Dosis dosis : givetDosis) {
			sum += dosis.getAntal();
		}
		return sum / antalDage();
	}

	@Override
	public String getType() {
		return "Daglig skæv";
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<Dosis>(givetDosis);
	}

}
