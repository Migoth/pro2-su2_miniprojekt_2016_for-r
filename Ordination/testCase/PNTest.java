package testCase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.DateTimeException;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;
import service.Service;

public class PNTest {
	private Service testCaseService = null;
	private PN pn = null;
	private Laegemiddel para = null;

	// ------------------------------------------------------------------------------------
	// PN Test cases

	@Before
	public void setUp() throws Exception {
		testCaseService = Service.getService();
		para = testCaseService.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

	}

	@Test
	public void testOpretPNObjekt() {
		pn = new PN(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 01), para, 1);
		assertNotNull(pn);
	}

	@Test(expected = DateTimeException.class)
	public void testOpretPNDosisUGyldigDato() {
		pn = new PN(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 12), para, 1);
		assertNull(pn.givDosis(LocalDate.of(2015, 01, 13)));
	}

	@Test
	public void testOpretPNDosisgyldig1() {
		pn = new PN(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 12), para, 1);
		int sizestart = pn.getAntalGangeGivet();
		pn.givDosis(LocalDate.of(2015, 01, 1));
		assertEquals(sizestart + 1, pn.getAntalGangeGivet(), 0);
	}

	@Test
	public void testOpretPNDosisgyldig2() {
		pn = new PN(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 12), para, 1);
		int sizestart = pn.getAntalGangeGivet();
		pn.givDosis(LocalDate.of(2015, 01, 12));
		assertEquals(sizestart + 1, pn.getAntalGangeGivet(), 0);
	}

	@Test
	public void TestPNGetAntalGangeGivetIngenDatoerGivet() {
		pn = new PN(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 12), para, 1);
		assertEquals(0, pn.getAntalGangeGivet(), 0);

	}

	@Test
	public void TestPnGetAntalGangeGivet3DatoerGivet() {
		pn = new PN(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 12), para, 1);
		pn.givDosis(LocalDate.of(2015, 01, 1));
		pn.givDosis(LocalDate.of(2015, 01, 1));
		pn.givDosis(LocalDate.of(2015, 01, 1));
		assertEquals(3, pn.getAntalGangeGivet(), 0);
	}

	@Test
	public void TestPnSamletDosisIngenDosisGivet() {
		pn = new PN(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 12), para, 1);
		assertEquals(0, pn.samletDosis(), 0);
	}

	@Test
	public void TestPnSamletDosisTreDosisGivet() {
		pn = new PN(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 12), para, 1);
		pn.givDosis(LocalDate.of(2015, 01, 1));
		pn.givDosis(LocalDate.of(2015, 01, 1));
		pn.givDosis(LocalDate.of(2015, 01, 1));
		assertEquals(3, pn.samletDosis(), 0);
	}

	@Test
	public void TestPNDoegnDosisIngenDosisGivet() {
		pn = new PN(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 12), para, 1);
		assertEquals(0, pn.doegnDosis(), 0);
	}

	@Test
	public void TestPnDoegnDosisTreDosisGivet() {
		pn = new PN(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 12), para, 1);
		pn.givDosis(LocalDate.of(2015, 01, 1));
		pn.givDosis(LocalDate.of(2015, 01, 1));
		pn.givDosis(LocalDate.of(2015, 01, 3));
		assertEquals(1, pn.doegnDosis(), 0);
	}

	@Test
	public void TestPNAntalDageIngenDatoerGivet() {
		pn = new PN(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 12), para, 1);
		assertEquals(0, pn.antalDage(), 0);
	}

	@Test
	public void TestPNAntalDage1DatoGivet() {
		pn = new PN(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 12), para, 1);
		pn.givDosis(LocalDate.of(2015, 01, 3));
		assertEquals(1, pn.antalDage(), 0);
	}

	@Test
	public void TestPNAntalDageTreDatoerGivet() {
		pn = new PN(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 12), para, 1);
		pn.givDosis(LocalDate.of(2015, 01, 1));
		pn.givDosis(LocalDate.of(2015, 01, 1));
		pn.givDosis(LocalDate.of(2015, 01, 3));
		assertEquals(3, pn.antalDage(), 0);
	}

}
