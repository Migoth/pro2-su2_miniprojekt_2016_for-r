package testCase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import service.Service;

public class ServiceTest {

	private Service testCast = null;
	private PN pn = null;
	private DagligFast dfo = null;
	private Patient pa = null;
	private DagligSkaev dso = null;
	private Laegemiddel para = null;

	// ------------------------------------------------------------------------------------
	// Service Test Cases

	@Before
	public void setUp() throws Exception {
		testCast = Service.getService();
		pa = testCast.opretPatient("Peter Pedersen", "121278-0511", 63.4);
		para = testCast.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

	}

	@Test(expected = IllegalArgumentException.class)
	public void TestOpretPNOrdinationSlutFoerStartDato() {
		pn = testCast.opretPNOrdination(LocalDate.of(2015, 1, 12), LocalDate.of(2015, 1, 1), pa, para, 1);
		assertEquals(0, pa.getOrdinationer().size(), 0);
	}

	@Test
	public void TestOpretPNOrdinationStartFoerSlutDato() {
		pn = testCast.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), pa, para, 1);
		assertEquals(1, pa.getOrdinationer().size(), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void TestOpretPNOrdinationIngenPatient() {
		pn = testCast.opretPNOrdination(LocalDate.of(2015, 1, 12), LocalDate.of(2015, 1, 1), null, para, 1);
		assertEquals(0, pa.getOrdinationer().size(), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void TestOpretPNOrdinationIngenLaegemiddel() {
		pn = testCast.opretPNOrdination(LocalDate.of(2015, 1, 12), LocalDate.of(2015, 1, 1), pa, null, 1);
		assertEquals(0, pa.getOrdinationer().size(), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void TestOpretPNOrdinationNegativMedicin() {
		pn = testCast.opretPNOrdination(LocalDate.of(2015, 1, 12), LocalDate.of(2015, 1, 1), pa, para, -1);
		assertEquals(0, pa.getOrdinationer().size(), 0);
	}

	// ----DagligFast----//
	@Test
	public void TestOpretDagligFastOrdinationGyldig1() {
		dfo = testCast.opretDagligFastOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), pa, para, 2, 4, 5,
				2);
		assertNotNull(dfo);
		assertEquals(1, pa.getOrdinationer().size(), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void TestOpretDagligFastOrdinationUGyldigDato() {
		dfo = testCast.opretDagligFastOrdination(LocalDate.of(2015, 1, 12), LocalDate.of(2015, 1, 1), pa, para, 2, 4, 5,
				2);
		assertNotNull(dfo);
		assertEquals(1, pa.getOrdinationer().size(), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void TestOpretDagligFastOrdinationIngenPatient() {
		dfo = testCast.opretDagligFastOrdination(LocalDate.of(2015, 1, 12), LocalDate.of(2015, 1, 1), null, para, 2, 4,
				5, 2);
		assertNotNull(dfo);
		assertEquals(1, pa.getOrdinationer().size(), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void TestOpretDagligFastOrdinationIngenMedicin() {
		dfo = testCast.opretDagligFastOrdination(LocalDate.of(2015, 1, 12), LocalDate.of(2015, 1, 1), pa, null, 2, 4, 5,
				2);
		assertNotNull(dfo);
		assertEquals(1, pa.getOrdinationer().size(), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void TestOpretDagligFastOrdinationUGyldigDosis1() {
		dfo = testCast.opretDagligFastOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), pa, para, -2, 4,
				5, 2);
		assertNull(dfo);
		assertEquals(1, pa.getOrdinationer().size(), 0);
	}

	// ----DagligSkæv----//
	@Test
	public void TestOpretDagligSkævOrdianationGyldig1() {
		LocalTime[] kl = { LocalTime.of(00, 0), LocalTime.of(02, 00), LocalTime.of(10, 0), LocalTime.of(15, 00) };
		double[] an = { 5, 2, 3, 6 };

		dso = testCast.opretDagligSkaevOrdination(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 15), pa, para, kl,
				an);
		assertEquals(4, dso.getDoser().size());
		assertEquals(1, pa.getOrdinationer().size(), 0);
	}

	@Test
	public void TestOpretDagligSkævOrdianationGyldig2() {
		LocalTime[] kl = { LocalTime.of(23, 59), LocalTime.of(02, 00), LocalTime.of(10, 0), LocalTime.of(15, 00) };
		double[] an = { 5, 2, 3, 6 };

		dso = testCast.opretDagligSkaevOrdination(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 15), pa, para, kl,
				an);
		assertEquals(4, dso.getDoser().size());
		assertEquals(1, pa.getOrdinationer().size(), 0);
	}

	@Test(expected = DateTimeException.class)
	public void TestOpretDagligSkævOrdianationuGyldigTid1() {
		LocalTime[] kl = { LocalTime.of(24, 0), LocalTime.of(02, 00), LocalTime.of(10, 0), LocalTime.of(15, 00) };
		double[] an = { 5, 2, 3, 6 };

		dso = testCast.opretDagligSkaevOrdination(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 15), pa, para, kl,
				an);
		assertEquals(4, dso.getDoser().size());
	}

	@Test(expected = IllegalArgumentException.class)
	public void TestOpretDagligSkævOrdianationuGyldigDosis1() {
		LocalTime[] kl = { LocalTime.of(00, 0), LocalTime.of(02, 00), LocalTime.of(10, 0), LocalTime.of(15, 00) };
		double[] an = { -5, 2, 3, 6 };

		dso = testCast.opretDagligSkaevOrdination(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 15), pa, para, kl,
				an);
		assertEquals(4, dso.getDoser().size());
	}

	@Test(expected = IllegalArgumentException.class)
	public void TestOpretDagligSkævOrdianationuGyldigDato() {
		LocalTime[] kl = { LocalTime.of(00, 0), LocalTime.of(02, 00), LocalTime.of(10, 0), LocalTime.of(15, 00) };
		double[] an = { -5, 2, 3, 6 };

		dso = testCast.opretDagligSkaevOrdination(LocalDate.of(2015, 01, 15), LocalDate.of(2015, 01, 1), pa, para, kl,
				an);
		assertEquals(4, dso.getDoser().size());
	}

	@Test(expected = IllegalArgumentException.class)
	public void TestOpretDagligSkævOrdianationIngenPatient() {
		LocalTime[] kl = { LocalTime.of(00, 0), LocalTime.of(02, 00), LocalTime.of(10, 0), LocalTime.of(15, 00) };
		double[] an = { -5, 2, 3, 6 };

		dso = testCast.opretDagligSkaevOrdination(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 15), null, para,
				kl, an);
		assertEquals(4, dso.getDoser().size());
	}

	@Test(expected = IllegalArgumentException.class)
	public void TestOpretDagligSkævOrdianationIngenLaegemiddel() {
		LocalTime[] kl = { LocalTime.of(00, 0), LocalTime.of(02, 00), LocalTime.of(10, 0), LocalTime.of(15, 00) };
		double[] an = { -5, 2, 3, 6 };

		dso = testCast.opretDagligSkaevOrdination(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 15), pa, null, kl,
				an);
		assertEquals(4, dso.getDoser().size());
	}

	// ------------------------------------------------------------------------------------
	// Ordination PN anvendt

	@Test(expected = IllegalArgumentException.class)
	public void TestOrdinationPNAnvendtDatoUdenforGyldigPeriode() {
		pn = testCast.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), pa, para, 1);
		testCast.ordinationPNAnvendt(pn, LocalDate.of(2015, 1, 13));
		assertEquals(1, pn.getAntalGangeGivet(), 0);
	}

	@Test
	public void TestOrdinationPNAnvendtDatoIndenforGyldigPeriode() {
		pn = testCast.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), pa, para, 1);
		testCast.ordinationPNAnvendt(pn, LocalDate.of(2015, 1, 12));
		assertEquals(1, pn.getAntalGangeGivet(), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void TestOrdinationPNAnvendtDatoUdenPN() {
		testCast.ordinationPNAnvendt(null, LocalDate.of(2015, 1, 13));
		assertEquals(1, pn.getAntalGangeGivet(), 0);
	}

	// ------------------------------------------------------------------------------------
	// Anbefalet dosis pr. døgn

	@Test(expected = IllegalArgumentException.class)
	public void TestAnbefaletDosisPrDoegnPatientVaegtUnderNul() {
		pa = testCast.opretPatient("Peter Pedersen", "121278-0511", -1);
		assertEquals(0, testCast.anbefaletDosisPrDoegn(pa, para), 0);
	}

	@Test
	public void TestAnbefaletDosisPrDoegnPatientVaegt24() {
		pa = testCast.opretPatient("Peter Pedersen", "121278-0511", 24);
		assertEquals(24, testCast.anbefaletDosisPrDoegn(pa, para), 0);
	}

	@Test
	public void TestAnbefaletDosisPrDoegnPatientVaegt25() {
		pa = testCast.opretPatient("Peter Pedersen", "121278-0511", 25);
		assertEquals(37.5, testCast.anbefaletDosisPrDoegn(pa, para), 0);
	}

	@Test
	public void TestAnbefaletDosisPrDoegnPatientVaegt120() {
		pa = testCast.opretPatient("Peter Pedersen", "121278-0511", 120);
		assertEquals(180, testCast.anbefaletDosisPrDoegn(pa, para), 0);
	}

	@Test
	public void TestAnbefaletDosisPrDoegnPatientVaeg121() {
		pa = testCast.opretPatient("Peter Pedersen", "121278-0511", 121);
		assertEquals(242, testCast.anbefaletDosisPrDoegn(pa, para), 0);
	}

	// ------------------------------------------------------------------------------------
	// Antal Ordinationer pr. Vægt pr. Lægemiddel

	@Test
	public void TestAntalOrdinationerPrVaegtPrLaegemiddel0Til24() {
		pn = testCast.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), pa, para, 1);
		assertEquals(0, testCast.antalOrdinationerPrVaegtPrLaegemiddel(0, 24, para), 0);
	}

	@Test
	public void TestAntalOrdinationerPrVaegtPrLaegemiddel25Til119() {
		pn = testCast.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), pa, para, 1);
		assertEquals(1, testCast.antalOrdinationerPrVaegtPrLaegemiddel(25, 119, para), 0);
	}

	@Test
	public void TestAntalOrdinationerPrVaegtPrLaegemiddel120Til150() {
		pa = testCast.opretPatient("Peter Pedersen", "121278-0511", 120.4);
		Patient pa2 = testCast.opretPatient("Peder Pedersen", "121278-0511", 149);
		pn = testCast.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), pa, para, 1);
		pn = testCast.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), pa2, para, 1);
		assertEquals(2, testCast.antalOrdinationerPrVaegtPrLaegemiddel(120, 150, para), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void TestAntalOrdinationerPrVaegtPrLaegemiddel120Til150IngenLaegemiddel() {
		pa = testCast.opretPatient("Peter Pedersen", "121278-0511", 120.4);
		Patient pa2 = testCast.opretPatient("Peder Pedersen", "121278-0511", 149);
		pn = testCast.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), pa, null, 1);
		pn = testCast.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), pa2, null, 1);
		assertEquals(2, testCast.antalOrdinationerPrVaegtPrLaegemiddel(120, 150, para), 0);
	}

}
