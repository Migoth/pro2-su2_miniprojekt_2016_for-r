package testCase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Dosis;
import ordination.Laegemiddel;
import ordination.Patient;
import service.Service;

public class DagligFastTest {
	private DagligFast df = null;
	private Laegemiddel para = null;
	private Patient pa = null;
	private Service testCast = null;
	private LocalTime morgen = LocalTime.of(06, 0);
	private LocalTime middag = LocalTime.of(12, 0);
	private LocalTime aften = LocalTime.of(18, 0);
	private LocalTime nat = LocalTime.of(23, 00);

	@Before
	public void setUp() throws Exception {
		testCast = Service.getService();
		pa = testCast.opretPatient("Peter Pedersen", "121278-0511", 63.4);
		para = testCast.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

	}

	@Test
	public void OpretDagligFastObjekt() {
		df = new DagligFast(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 15));
		assertNotNull(df);
	}

	@Test
	public void OpretDosisGyldigDosis() {
		df = new DagligFast(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 15));
		df.opretDosis(morgen, 2);
		df.opretDosis(middag, 4);
		df.opretDosis(aften, 5);
		df.opretDosis(nat, 2);
		Dosis[] dosistemp = df.getDoser();
		assertNotNull(dosistemp[0]);
		assertNotNull(dosistemp[1]);
		assertNotNull(dosistemp[2]);
		assertNotNull(dosistemp[3]);
	}

	@Test(expected = IllegalArgumentException.class)
	public void OpretDosisUgyldigDosis() {
		df = new DagligFast(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 15));
		df.opretDosis(morgen, -2);
		df.opretDosis(middag, 4);
		df.opretDosis(aften, 5);
		df.opretDosis(nat, 2);
		Dosis[] dosistemp = df.getDoser();
		assertNotNull(dosistemp[0]);
		assertNotNull(dosistemp[1]);
		assertNotNull(dosistemp[2]);
		assertNotNull(dosistemp[3]);
	}

	@Test(expected = DateTimeException.class)
	public void OpretDosisUgyldigKlokkeslaet() {
		df = new DagligFast(LocalDate.of(2015, 01, 01), LocalDate.of(2015, 01, 15));
		df.opretDosis(morgen, 2);
		df.opretDosis(middag, 4);
		df.opretDosis(aften, 5);
		df.opretDosis(LocalTime.of(24, 00), 2);
		Dosis[] dosistemp = df.getDoser();
		assertNotNull(dosistemp[0]);
		assertNotNull(dosistemp[1]);
		assertNotNull(dosistemp[2]);
		assertNotNull(dosistemp[3]);
	}

	@Test
	public void TestDagligFastSamletDosisIngenDosis() {
		DagligFast df = new DagligFast(LocalDate.of(2015, 01, 10), LocalDate.of(2015, 01, 12), para);
		double sum = df.samletDosis();
		assertEquals(0, sum, 0);
	}

	@Test
	public void TestDagligFastSamletDosis3Dage() {
		DagligFast df = testCast.opretDagligFastOrdination(LocalDate.of(2016, 1, 10), LocalDate.of(2016, 1, 12), pa,
				para, 2, 1, 1, 1);
		double sum = df.samletDosis();
		assertEquals(15, sum, 0);
	}

	@Test
	public void TestDagligFastDøgnDosisIngenDosis() {
		DagligFast df = new DagligFast(LocalDate.of(2015, 01, 10), LocalDate.of(2015, 01, 12), para);
		double sum = df.doegnDosis();
		assertEquals(0, sum, 0);
	}

	@Test
	public void TestDagligFastDøgnDosis3Dage() {
		DagligFast df = testCast.opretDagligFastOrdination(LocalDate.of(2016, 1, 10), LocalDate.of(2016, 1, 12), pa,
				para, 2, 1, 1, 1);
		double sum = df.doegnDosis();
		assertEquals(5, sum, 0);
	}

}
