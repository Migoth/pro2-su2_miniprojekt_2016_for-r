package testCase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Laegemiddel;
import service.Service;

public class DagligSkaevTest {
	private Service testCast = null;
	private Laegemiddel para = null;

	@Before
	public void setUp() throws Exception {
		testCast = Service.getService();
		para = testCast.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

	}

	@Test

	public void opretDagligSkaev() {
		DagligSkaev dso = new DagligSkaev(LocalDate.of(2015, 01, 1), LocalDate.of(2015, 01, 12), para);
		assertNotNull(dso);
	}

	@Test
	public void opretDosisAntal1() {
		DagligSkaev dso = new DagligSkaev(LocalDate.of(2015, 01, 1), LocalDate.of(2015, 01, 12), para);
		double an = 1;
		LocalTime kl = LocalTime.of(23, 50);
		dso.opretDosis(kl, an);
		assertEquals(1, dso.getDoser().size(), 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void opretDosisAntalMinus1() {
		DagligSkaev dso = new DagligSkaev(LocalDate.of(2015, 01, 1), LocalDate.of(2015, 01, 12), para);
		double an = -1;
		LocalTime kl = LocalTime.of(23, 50);
		dso.opretDosis(kl, an);
		assertEquals(1, dso.getDoser().size(), 0);
	}

	@Test(expected = DateTimeException.class)
	public void opretDosisForkertKlokkeslet() {
		DagligSkaev dso = new DagligSkaev(LocalDate.of(2015, 01, 1), LocalDate.of(2015, 01, 12), para);
		double an = 1;
		LocalTime kl = LocalTime.of(24, 00);
		dso.opretDosis(kl, an);
		assertEquals(1, dso.getDoser().size(), 0);
	}

	public void opretDagligSkævGyldigDato() {
		DagligSkaev dso = new DagligSkaev(LocalDate.of(2015, 01, 1), LocalDate.of(2015, 01, 12), para);
		assertNotNull(dso);

	}

	// ----------------------------------------------------------------------------------
	// Samlet dosis

	@Test
	public void samletDosisIngenDosis() {
		DagligSkaev dso = new DagligSkaev(LocalDate.of(2015, 01, 1), LocalDate.of(2015, 01, 12), para);
		assertEquals(0, dso.samletDosis(), 0);
	}

	@Test
	public void samletDosisEnDosis() {
		DagligSkaev dso = new DagligSkaev(LocalDate.of(2015, 01, 1), LocalDate.of(2015, 01, 12), para);
		double an = 1;
		LocalTime kl = LocalTime.of(23, 50);
		dso.opretDosis(kl, an);
		assertEquals(1, dso.samletDosis(), 0);
	}

	// ----------------------------------------------------------------------------------
	// Døgn Dosis

	@Test
	public void doegnDosisIngenDosis() {
		DagligSkaev dso = new DagligSkaev(LocalDate.of(2015, 01, 1), LocalDate.of(2015, 01, 1), para);
		assertEquals(0, dso.doegnDosis(), 0);
	}

	@Test
	public void doegnDosisToDosis() {
		DagligSkaev dso = new DagligSkaev(LocalDate.of(2015, 01, 1), LocalDate.of(2015, 01, 1), para);
		double an1 = 2.0;
		LocalTime kl1 = LocalTime.of(20, 50);
		dso.opretDosis(kl1, an1);
		double an2 = 1.0;
		LocalTime kl2 = LocalTime.of(23, 50);
		dso.opretDosis(kl2, an2);
		assertEquals(3, dso.doegnDosis(), 0);
	}

}
